<div align="center"><img width="200" src="https://gitee.com/chu1204505056/vue-admin-beautiful/raw/master/src/colorfulIcon/svg/vab.svg"/>
<h1> vue-admin-beautiful（element-ui） </h1>
</div>

## 地址

- [🎉 vue2.x + element-ui（免费商用，支持 PC、平板、手机）](http://beautiful.panm.cn/vue-admin-beautiful-element/?hmsr=github&hmpl=&hmcu=&hmkw=&hmci=)

- [⚡️ vue3.x + element-plus（alpha 版本，免费商用，支持 PC、平板、手机）](http://beautiful.panm.cn/vue-admin-beautiful-element-plus/?hmsr=github&hmpl=&hmcu=&hmkw=&hmci=)

- [⚡️ vue3.x + ant-design-vue（beta 版本，免费商用，支持 PC、平板、手机）](http://beautiful.panm.cn/vue-admin-beautiful-antdv/?hmsr=github&hmpl=&hmcu=&hmkw=&hmci=)

- [🚀 pro 版演示地址（vue2.x 付费版本，支持 PC、平板、手机）](http://beautiful.panm.cn/admin-pro/?hmsr=github&hmpl=&hmcu=&hmkw=&hmci=)

- [🚀 plus 版演示地址（vue3.x 付费版本，支持 PC、平板、手机）](http://beautiful.panm.cn/admin-plus/?hmsr=github&hmpl=&hmcu=&hmkw=&hmci=)

- [📌 pro 及 plus 购买地址 authorization](http://beautiful.panm.cn/authorization/)

- [🌐 github 仓库地址](https://github.com/chuzhixin/vue-admin-beautiful/?hmsr=github&hmpl=&hmcu=&hmkw=&hmci=)

- [🌐 码云仓库地址](https://gitee.com/chu1204505056/vue-admin-beautiful/?hmsr=github&hmpl=&hmcu=&hmkw=&hmci=)

## 🌱 vue3.x vue3.0-antdv 分支（ant-design-vue）[点击切换分支](https://github.com/chuzhixin/vue-admin-beautiful/tree/vue3.0-antdv)

```bash
# 克隆项目
git clone -b vue3.0-antdv https://github.com/chuzhixin/vue-admin-beautiful.git
# 进入项目目录
cd vue-admin-beautiful
# 安装依赖
npm i
# 本地开发 启动项目
npm run serve
```

## 🌱vue2.x master 分支（element-ui）[点击切换分支](https://github.com/chuzhixin/vue-admin-beautiful/tree/master)

```bash
# 克隆项目
git clone -b master https://github.com/chuzhixin/vue-admin-beautiful.git
# 进入项目目录
cd vue-admin-beautiful
# 安装依赖
npm i
# 本地开发 启动项目
npm run serve
```

## 友情链接

- [uView 文档（超棒的移动跨端框架，文档详细，上手容易）](https://uviewui.com/)

- [uView 开源地址（uni-app 生态优秀的 UI 框架，全面的组件和便捷的工具会让您信手拈来，如鱼得水）](https://github.com/YanxinNet/uView/)

- [Element UI 表单设计及代码生成器（可视化表单设计器，一键生成 element 表单）](https://github.com/JakHuang/form-generator/)

- [luch-request（基于 Promise 开发的 uni-app 跨平台、项目级别的请求库）](https://www.quanzhan.co/luch-request/)

- [umyui](http://www.umyui.com/)

## 我们承诺将定期赞助的开源项目（感谢巨人）

<a title="vue" href="https://cn.vuejs.org/" target="_blank">
<img width="64px" src="https://gitee.com/chu1204505056/image/raw/master/vue.png"/>
</a>

<a title="ant-design-vue" href="https://github.com/vueComponent/ant-design-vue#backers" target="_blank">
<img width="64px" src="https://gitee.com/chu1204505056/image/raw/master/antdv.svg"/>
</a>

<a title="element-plus" href="https://opencollective.com/element-plus" target="_blank">
<img width="64px" src="https://gitee.com/chu1204505056/image/raw/master/element-plus.png"/>
</a>

## 鸣谢

| Project                                                          |
| ---------------------------------------------------------------- |
| [vue](https://github.com/vuejs/vue)                              |
| [element-ui](https://github.com/ElemeFE/element)                 |
| [element-plus](https://github.com/element-plus/element-plus)     |
| [ant-design-vue](https://github.com/vueComponent/ant-design-vue) |
| [mock](https://github.com/nuysoft/Mock)                          |
| [axios](https://github.com/axios/axios)                          |
