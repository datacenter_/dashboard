// import request from '../../axios/request'
import request from '@/utils/request'

// 注册
export function registrator(data) {
  return request({
    url: '/api/admin/auth/registrator',
    method: 'post',
    data
  })
}
// 注册-发送邮件
export function register_send_email(data) {
  return request({
    url: '/api/admin/auth/register-send-email',
    method: 'post',
    data
  })
}
// 忘记密码-发送邮件
export function send_email(data) {
  return request({
    url: '/api/admin/auth/send-email',
    method: 'post',
    data
  })
}
// 忘记密码-提交重置密码
export function modify_password(data) {
  return request({
    url: '/api/admin/auth/modify-password',
    method: 'post',
    data
  })
}
// 获取token
export function getToken(token) {
  return request({
    url: '/api/admin/secret/get',
    method: 'get'
  })
}
// 生成token
export function token_add(data) {
  return request({
    url: '/api/admin/secret/add',
    method: 'post',
    data
  })
}
// 撤销token再次新增一条token
export function getTokenAgain(data) {
  return request({
    url: '/api/admin/secret/revoke',
    method: 'get',
    params: data
  })
}
// 个人认证
export function attestation_personal(data) {
  return request({
    url: '/api/admin/attestation/personal',
    method: 'post',
    data
  })
}
// 公司认证
export function attestation_enterprise(data) {
  return request({
    url: '/api/admin/attestation/enterprise',
    method: 'post',
    data
  })
}
// 重置邮箱-验证密码-发送邮件
export function reset_send_email(data) {
  return request({
    url: '/api/admin/personal-center/reset-send-email',
    method: 'post',
    data
  })
}
// 重置邮箱-验证原邮箱验证码
export function reset_email_code(data) {
  return request({
    url: '/api/admin/personal-center/reset-email-code',
    method: 'post',
    data
  })
}
// 重置邮箱-新邮箱-发送验证码
export function reset_email_send(data) {
  return request({
    url: '/api/admin/personal-center/reset-email-send',
    method: 'post',
    data
  })
}
// 重置邮箱-新邮箱验证验证码-修改邮箱
export function new_email_save(data) {
  return request({
    url: '/api/admin/personal-center/new-email-code-save',
    method: 'post',
    data
  })
}
// token-生效/撤销
export function token_edit(data) {
  return request({
    url: '/api/admin/secret/token-edit',
    method: 'get',
    params: data
  })
}
// 完善个人信息
export function new_password(data) {
  return request({
    url: '/api/admin/personal-center/improve-information',
    method: 'post',
    data
  })
}
// 获取地区信息
export function regions(data) {
  return request({
    url: '/api/admin/attestation/regions',
    method: 'get',
    params: data
  })
}
// 认证信息获取
export function attestation_detail(data) {
  return request({
    url: '/api/admin/attestation/detail',
    method: 'get',
    params: data
  })
}
// 完善个人信息最新
export function account_information(data) {
  return request({
    url: '/api/admin/personal-center/account-information',
    method: 'post',
    data
  })
}
// 用户数据权限
export function data_power(data) {
  return request({
    url: '/api/admin/user/data-power',
    method: 'get',
    params: data
  })
}
// 用户访问数据统计日志记录
export function data_statistics_record(data) {
  return request({
    url: '/api/admin/user/data-statistics-record',
    method: 'get',
    params: data
  })
}
// 用户访问数据日志记录
export function data_user_record(data) {
  return request({
    url: '/api/admin/user/data-record',
    method: 'get',
    params: data
  })
}
