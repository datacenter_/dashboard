import Vue from 'vue';
import locale from 'element-ui/lib/locale';
import VueI18n from 'vue-i18n';
import enLocale from 'element-ui/lib/locale/lang/en'
import zhLocale from 'element-ui/lib/locale/lang/zh-CN'

import {BootstrapVue, IconsPlugin} from 'bootstrap-vue'
// import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap-vue/dist/bootstrap-vue.css'
import en1 from './en';
import zh1 from './zh';
import en2 from './language/en'
import zh2 from './language/zh'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueI18n);

const i18n = new VueI18n({
  locale: localStorage.getItem('languageSet') || 'zh', //从localStorage里获取用户中英文选择，没有则默认中文

  messages: {
    en: {
      message: 'hello',
      ...en1, //英文
      ...enLocale,
      ...en2
    },
    zh: {
      message: '你好',
      ...zh1, //中文
      ...zhLocale,
      ...zh2
    }
  },
  silentTranslationWarn: true //去掉警告
});
// 表单验证
// 您必须配置vee-validate的 fields属性，否则它在注入自身时将与<b-table>的： :fieldss属性（可能还有其他组件）冲突。
import VeeValidate, {Validator} from "vee-validate";
import zh from 'vee-validate/dist/locale/zh_CN'
import en from 'vee-validate/dist/locale/en'
// 初始化表单验证，同时使用i18n
Vue.use(VeeValidate,{
  i18n,
  i18nRootKey: 'validation',
  dictionary: {
    zh,
    en
  },
  // This is the default
  inject: true,
  // Important to name this something other than 'fields'
  fieldsBagName: 'veeFields',
  // This is not required but avoids possible naming conflicts
  errorBagName: 'veeErrors'
})
Validator.localize('zh', {
  messages: zh.messages,
})
Validator.localize('en', {
  messages: en.messages,
})
export function setMessage(validName, errMsgZh, errMsgEn, validate) {
  console.log('语言', Validator.locale)
  if (Validator.locale == 'en') {
    // 自定义验证规则,取名为validName, 通过该方式使用v-validate="'required|phone'"   v-validate.continues="'phoneNum'"实现必填
    Validator.extend(validName, {
      // 提示信息，不符合规则提示语
      getMessage: (field, [args]) => {
        return errMsgEn
      },
      // 验证规则，符合规则通过，否则不通过 (规则为美国电话号码)
      validate: validate
    })
  }
  if (Validator.locale == 'zh') {
    Validator.extend(validName, {
      getMessage: (field, [args]) => {
        return errMsgZh
      },
      validate: validate
    })
  }

}
// 修改默认错误提示
const dict = {
  zh: {messages: {email: (name) => `${name}不合法`}},
  en: {messages: {email: (name) => `${name} is invalid`}},
}
Validator.localize(dict)
const dict1 = {
  zh: {messages: {required: (name) => `必须填写`}},
  en: {messages: {required: (name) => `Field required`}},
}
Validator.localize(dict1)
const dict2 = {
  zh: {messages: {confirmed: (name) => `两次输入的密码不一致`}},
  en: {messages: {confirmed: (name) => `Password doesn't match`}},
}
Validator.localize(dict2)
// 添加规则
// 注册提示
setMessage('requireds', '必须填写', 'Field required', (value, [args]) => {
  const reg = /\S/;
  return reg.test(value)
})
setMessage('y_password', '密码必须为8-16位字母和数字组合', 'The password must be a combination of 8-16 letters and numbers', (value, [args]) => {
  const reg = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,16}$/;
  return reg.test(value)
})
setMessage('y_tmp', '电话格式错误', 'Telephone format error', (value, [args]) => {
  const reg = /[\d]([-])*$/;
  return reg.test(value)
})
// 登录提示

// setMessage('phoneNum1', '手机号不正确', 'phoneNum error', (value, [args]) => {
//   const reg = /^((13|14|15|17|18)[0-9]{1}\d{8})$/
//   return reg.test(value)
// })
// setMessage('personName', '姓名只能包括中文字或英文字母', 'username no yes', (value, [args]) => {
//   const reg = /^([\u4e00-\u9fa5\s]|[a-zA-Z])*$/
//   return reg.test(value)
// })



locale.i18n((key, value) => i18n.t(key, value)); //重点：为了实现element插件的多语言切换

export default i18n;
