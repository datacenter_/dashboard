import Layout from '@/layouts'

const gamecenterRouters = [
  {
    path: '/',
    component: Layout,
    redirect: '/index',
    children: [
      {
        path: '/index',
        name: 'Index',
        component: () => import('@/views/index/index'),
        meta: {
          title: 'AccessToken',  //密匙管理
          icon: 'home',
          affix: true,
          noKeepAlive: true,
        },
      },
    ],
  },
  {
    path: '/dashboad',
    component: Layout,
    redirect: 'noRedirect',
    name: 'dashboad',
    meta: {title: 'AccessScopes', icon: 'users-cog'}, //数据权限
    // meta: {title: '客户权限管理', icon: 'users-cog', permissions: ['admin']},
    children: [
      {
        path: '/gamecenter/scopes_list',
        name: 'scopes_list',
        meta: {title: 'AccessScopes'},
        component: () => import('@/views/gamecenter/scopes_list'),
      },
    ]
  },
  {
    path: '/dashboad',
    component: Layout,
    redirect: 'noRedirect',
    name: 'dashboad',
    meta: {title: 'DataUsage', icon: 'users-cog'}, //数据用途
    // meta: {title: '客户权限管理', icon: 'users-cog', permissions: ['admin']},
    children: [
      {
        path: '/gamecenter/api_usage_list',
        name: 'api_usage_list',
        meta: {title: 'DataUsage'},
        component: () => import('@/views/gamecenter/api_usage_list')
      },
    ]
  },
  {
    path: '/dashboad',
    component: Layout,
    redirect: 'noRedirect',
    name: 'dashboad',
    meta: {title: 'APIChangelog', icon: 'users-cog'}, //API变更录
    // meta: {title: '客户权限管理', icon: 'users-cog', permissions: ['admin']},
    children: [
      {
        path: '/gamecenter/api_changelog_list',
        name: 'api_changelog_list',
        meta: {title: 'APIChangelog'},
        component:  () => import('@/views/gamecenter/api_changelog_list')
      },
    ]
  },
  {
    path: '/dashboad',
    component: Layout,
    redirect: 'noRedirect',
    name: 'dashboad',
    meta: {title: 'Account', icon: 'users-cog'}, //个人中心
    // meta: {title: '客户权限管理', icon: 'users-cog', permissions: ['admin']},
    children: [
      {
        path: '/gamecenter/center_list',
        name: 'center_list',
        meta: {title: 'Profile'}, //账号信息
        component: () => import('@/views/gamecenter/center_list')
      },
      {
        path: '/gamecenter/change_password',
        name: 'change_password',
        meta: {title: 'Accountsecurity'}, //账号安全
        component:  () => import('@/views/gamecenter/change_password')
      },
      {
        path: '/gamecenter/change_email_list',
        name: 'change_email_list',
        meta: {title: 'MailboxSetting'}, //更换邮箱
        component: () => import('@/views/gamecenter/change_email_list'),
        hidden: true
      },
    ]
  },
]
export default gamecenterRouters
