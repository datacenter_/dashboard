import Layout from '@/layouts'

const gamecenterRouters = [
  {
    path: '/elementsdata/clientRules',
    component: Layout,
    redirect: 'noRedirect',
    name: 'elementsdata',
    meta: {title: 'Rules', icon: 'users-cog'},
    // meta: {title: '客户权限管理', icon: 'users-cog', permissions: ['admin']},
    children: [
      {
        path: 'index',
        name: 'clientRules',
        component: () =>
          import('@/views/elementsdata/clientRules/index'),
        meta: {title: 'Rules'},
      },
      {
        path: 'detail',
        name: 'detail',
        component: () =>
          import('@/views/elementsdata/clientRules/detail'),
        meta: {title: 'Detail'},
        hidden: true
      },
      {
        path: 'privateTournament',
        name: 'privateTournament',
        component: () =>
          import('@/views/elementsdata/clientRules/privateTournament'),
        meta: {title: 'PrivateTournament'},
        hidden: true
      },
      {
        path: 'add',
        name: 'privateTournamentAdd',
        component: () =>
          import('@/views/elementsdata/clientRules/add'),
        meta: {title: 'Add'},
        hidden: true
      },
      {
        path: 'edit',
        name: 'privateTournamentedit',
        component: () =>
          import('@/views/elementsdata/clientRules/edit'),
        meta: {title: 'Edit'},
        hidden: true
      },
    ]
  },
]
export default gamecenterRouters

