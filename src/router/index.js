/**
 * @copyright chuzhixin 1204505056@qq.com
 * @description router全局配置，如有必要可分文件抽离
 */

import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '@/layouts/dashboardLayout'
import EmptyLayout from '@/layouts/EmptyLayout'
import { publicPath, routerMode } from '@/config'
Vue.use(VueRouter)

import scopes_list from '../views/gamecenter/scopes_list'
import api_usage_list from '../views/gamecenter/api_usage_list'
import api_changelog_list from '../views/gamecenter/api_changelog_list'
import center_list from '../views/gamecenter/center_list'
import change_email_list from '../views/gamecenter/change_email_list'
import change_password from '../views/gamecenter/change_password'

export const constantRoutes = [
  // {
  //   path: '/',
  //   component: Layout,
  //   // meta: {title: '客户权限管理', icon: 'users-cog', permissions: ['admin']},
  //   children: [
  //     {
  //       path: 'login',
  //       component: () => import('@/views/loginGames/index'),
  //       hidden: true,
  //     },
  //   ]
  // },
  {
    path: '/login',
    component: () => import('@/views/loginGames/index'),
    hidden: true,
  },
  {
    path: '/register',
    component: () => import('@/views/loginGames/register/index'),
    hidden: true,
  },
  {
    path: '/401',
    name: '401',
    component: () => import('@/views/401'),
    hidden: true,
  },
  {
    path: '/404',
    name: '404',
    component: () => import('@/views/404'),
    hidden: true,
  },
]

/*当settings.js里authentication配置的是intelligence时，views引入交给前端配置*/
// import elementsData from './elementsData'
import dashboard from './dashboard'
export const asyncRoutes = [
  ...dashboard,
  // ...elementsData,
  {
    path: '*',
    redirect: '/404',
    hidden: true,
  },
]

const router = new VueRouter({
  base: routerMode === 'history' ? publicPath : '',
  mode: routerMode,
  scrollBehavior: () => ({
    y: 0,
  }),
  routes: constantRoutes,
})
//注释的地方是允许路由重复点击，如果你觉得框架路由跳转规范太过严格可选择放开
/* const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location, onResolve, onReject) {
  if (onResolve || onReject)
    return originalPush.call(this, location, onResolve, onReject);
  return originalPush.call(this, location).catch((err) => err);
}; */

export function resetRouter() {
  router.matcher = new VueRouter({
    base: routerMode === 'history' ? publicPath : '',
    mode: routerMode,
    scrollBehavior: () => ({
      y: 0,
    }),
    routes: constantRoutes,
  }).matcher
}

export default router
