const state = {
language: ''
};

const mutations = {
  setLanguage(state, language) {
    if (language) {
      state.language = language;
    } else {
      state.language = '';
    }
  }
};

const actions = {
  setLanguage({commit}, language) {
    commit('setLanguage', language);
  }
};

export default {
  state,
  mutations,
  actions
};
