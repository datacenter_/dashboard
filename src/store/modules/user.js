/**
 * @author chuzhixin 1204505056@qq.com （不想保留author可删除）
 * @description 登录、获取用户信息、退出登录、清除accessToken逻辑，不建议修改
 */

import Vue from 'vue'
import { getUserInfo, login, logout } from '../../api/user'
import {
  getAccessToken,
  removeAccessToken,
  setAccessToken,
} from '@/utils/accessToken'
import { resetRouter } from '@/router'
import { title, tokenName } from '@/config'

const state = () => ({
  accessToken: getAccessToken(),
  username: '',
  avatar: '',
  permissions: [],
  groupId: '', // 权限
  email: '',

})
const getters = {
  accessToken: (state) => state.accessToken,
  username: (state) => state.username,
  avatar: (state) => state.avatar,
  email: (state) => state.email,
  permissions: (state) => state.permissions,
  groupId: (state) => state.groupId,
}
const mutations = {
  setAccessToken(state, accessToken) {
    state.accessToken = accessToken
    setAccessToken(accessToken)
  },
  setUsername(state, username) {
    state.username = username
  },
  setAvatar(state, avatar) {
    state.avatar = avatar
  },
  groupId(state, groupId) {
    state.groupId = groupId
  },
  email(state, email) {
    state.email = email
  },
  setPermissions(state, permissions) {
    state.permissions = permissions
  },
}
const actions = {
  setPermissions({ commit }, permissions) {
    commit('setPermissions', permissions)
  },
  login({ commit }, userInfo) {
    const { username, password} = userInfo
    return new Promise((resolve, reject) => {
      login({ email: username.trim(), password: password }).then(response => {
        const { data } = response // 解构出data

        const accessToken = data.token
        if (accessToken) {
          commit('setAccessToken', accessToken)
          const hour = new Date().getHours()
          const thisTime =
            hour < 8
              ? '早上好'
              : hour <= 11
              ? '上午好'
              : hour <= 13
                ? '中午好'
                : hour < 18
                  ? '下午好'
                  : '晚上好'
          Vue.prototype.$baseNotify(`欢迎登录${title}`, `${thisTime}！`)
        }
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },
  // async login({ commit }, userInfo) {
  //   const { data } = await login(userInfo)
  //   console.log('登录',data)
  //   const accessToken = data[tokenName]
  //   if (accessToken) {
  //     commit('setAccessToken', accessToken)
  //     const hour = new Date().getHours()
  //     const thisTime =
  //       hour < 8
  //         ? '早上好'
  //         : hour <= 11
  //         ? '上午好'
  //         : hour <= 13
  //           ? '中午好'
  //           : hour < 18
  //             ? '下午好'
  //             : '晚上好'
  //     Vue.prototype.$baseNotify(`欢迎登录${title}`, `${thisTime}！`)
  //   } else {
  //     Vue.prototype.$baseMessage(
  //       `登录接口异常，未正确返回${tokenName}...`,
  //       'error'
  //     )
  //   }
  // },

  async getUserInfo({ commit, state }) {
    const { data } = await getUserInfo(state.accessToken)
    console.log('获取用户信息', data)
    if (!data) {
      Vue.prototype.$baseMessage('验证失败，请重新登录...', 'error')
      return false
    }
    data['avatar'] = 'https://i.gtimg.cn/club/item/face/img/2/15922_100.gif' // 头像
    data['permissions'] = ['admin'] // 权限
    let { permissions, username, avatar, group_id, email } = data
    console.log('权限你好', data)
    commit('groupId', group_id)
    commit('email', email)
    if (permissions && username?username:'新用户' && Array.isArray(permissions)) {
      commit('setPermissions', permissions)
      commit('setUsername', username)
      commit('setAvatar', avatar)
      return permissions
    } else {
      Vue.prototype.$baseMessage('用户信息接口异常', 'error')
      return false
    }
  },
  async logout({ dispatch }) {
    await logout(state.accessToken)
    await dispatch('resetAccessToken')
    await resetRouter()
  },
  resetAccessToken({ commit }) {
    commit('setPermissions', [])
    commit('setAccessToken', '')
    removeAccessToken()
  },
}
export default { state, getters, mutations, actions }
